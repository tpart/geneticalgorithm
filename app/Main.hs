module Main (main) where

import System.Random
import qualified Data.List as L
import qualified Data.List.Split as L

type Genotype = [Bool]
type Population = [Genotype]
type Phenotype = String
type Score = Int

main :: IO ()
main = do
    let gs = fmap mkStdGen [0..(extinctionCount - 1)]
    mapM_ (putStrLn . toPheno . last) (generations gs initialPopulation)

populationCount :: Int
populationCount = 64

preservationCount :: Int
preservationCount = 10

target :: Phenotype
target = "Hello World"

extinctionCount :: Int
extinctionCount = populationCount - preservationCount

initialPopulation :: Population
initialPopulation = replicate populationCount $ replicate geneLength False

mutate :: RandomGen g => g -> Genotype -> (Genotype, g)
mutate g gene = if a == 0 then (modified, newG2) else mutate newG modified
    where modified = mapOn not i gene
          (i, newG2) = uniformR (0, geneLength - 1) newG
          (a, newG) = uniformR (0 :: Int, 1) g

generations :: RandomGen g => [g] -> Population -> [Population]
generations gs p = newPop : generations newGs newPop
    where (newPop, newGs) = reproduce gs p

reproduce :: RandomGen g => [g] -> Population -> (Population, [g])
reproduce gs p = (newPop ++ best, newGs)
    where best = drop extinctionCount $ L.sortOn (fitness . toPheno) $ shuffle (head gs) p
          (newPop, newGs) = unzip $ zipWith mutate gs $ replicate extinctionCount $ last best

shuffle :: RandomGen g => g -> [a] -> [a]
shuffle _ [a] = [a]
shuffle g as = as !! i : shuffle newG (dropI i as)
    where (i, newG) = uniformR (0, length as - 1) g

dropI :: Int -> [a] -> [a]
dropI k = fmap snd . filter ((k /=) . fst) . zip [0..]

-- Select fitness function here
fitness :: Phenotype -> Score
fitness = fitnessTarget

fitnessTarget :: Phenotype -> Score
fitnessTarget s = length $ filter id $ zipWith (==) s $ padTo16 target

-- fitnessUnique :: Phenotype -> Score
-- fitnessUnique = length . L.nub

-- fitnessSequential :: Phenotype -> Score
-- fitnessSequential (a:b:bs) = fromEnum (succ a == b) + fitnessSequential (b:bs)
-- fitnessSequential _ = 0

padTo16 :: String -> String
padTo16 s | length s > 16 = error "string too long"
padTo16 s = s ++ replicate (16 - length s) '.'

geneLength :: Int
geneLength = 6 * 16

toPheno :: Genotype -> Phenotype
toPheno = fmap (toChar . toNumber) . L.chunksOf 6

toNumber :: [Bool] -> Int
toNumber = sum . fmap (\(a,b) -> if b then 2 ^ a else 0) . exponentList
    where
        exponentList :: [Bool] -> [(Int, Bool)]
        exponentList = reverse . zip [0..] . reverse

toChar :: Int -> Char
toChar i = (['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9'] ++ ['.',' ']) !! (i `mod` 64)

mapOn :: (a -> a) -> Int -> [a] -> [a]
mapOn f i = zipWith (\a b -> if a == i then f b else b) [0..]
